﻿using scafold.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace scafold.DAL
{
    public class BookInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            var author = new Author
            {
                Biography = "Hiç sevmediğim bir insandır",
                FirstName = "Gabriel",
                LastName = "Márquez"
            };
            var books = new List<Book>
             {
                 new Book {
                 Author = author,
                 Description = "İyi bir kitap",
                 ImageUrl = "https://img.theculturetrip.com/768x/images/56-3688849-page6-pasted-graphic.jpg",
                 Isbn = "1491914319",
                 Synopsis = "...",
                 Title = "One Hundred Years of Solitude"
                 },
                 new Book {
                 Author = author,
                 Description = "...",
                 ImageUrl = "https://img.theculturetrip.com/768x/images/56-3688849-page6-pasted-graphic.jpg",
                 Isbn = "1449319548",
                 Synopsis = "...",
                 Title = "Chronicle of a Death Foretold"
                 },
                 new Book {
                 Author = author,
                 Description = "...",
                 ImageUrl = "https://img.theculturetrip.com/768x/images/56-3688849-page6-pasted-graphic.jpg",
                 Isbn = "1449309860",
                 Synopsis = "...",
                 Title = "Love in the Time of Cholera"
                 },
                 new Book {
                 Author = author,
                 Description = "...",
                 ImageUrl = "https://img.theculturetrip.com/768x/images/56-3688849-page6-pasted-graphic.jpg",
                 Isbn = "1460954394",
                 Synopsis = "...",
                 Title = "Of Love and Other Demons"
             }
            };
            books.ForEach(b => context.Books.Add(b));
            context.SaveChanges();
        }
    }

}
